#!/bin/bash

rm ~/.bashrc
ln -s ~/.config/.bashrc ~/.bashrc

rm ~/.inputrc
ln -s ~/.config/.inputrc ~/.inputrc
